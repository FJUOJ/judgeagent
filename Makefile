ver=release

ROOTDIR=$(shell pwd)

OUTDIR=.
OUTFILE=agent

ALL:
	CGO_ENABLED=0 go build --ldflags "-extldflags -static" -v -p 4 -o $(OUTDIR)/$(OUTFILE) ./*.go 
clean:
	rm -f ./agent