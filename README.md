# Judge Agent

### Intro

Put this binary into ```/srv/agent``` when you build a FJUOJ env.

It will mesure the time execute when the binary run at ```/srv/main```, read the stdin from ```/opt/in``` put the output to ```/opt/out```, put time info to ```/opt/info```


### Note 

It will have ~8 ms latency, but it okay for judge system  

### Compile

```make```

### Usage

```/srv/agent```
