package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"syscall"
	"time"
)

func main() {

	args := os.Args[1:]
	var startTime time.Time
	var endTime time.Time
	var cmd *exec.Cmd

	infoFile, err := os.Create("/opt/info")

	if err != nil {
		panic(err)
	}

	if len(args) > 1 {

		firstArg := args[0]
		afterArg := args[1:]
		cmd = exec.Command(firstArg, afterArg...)

	} else {

		firstArg := args[0]
		cmd = exec.Command(firstArg)

	}

	inFile, _ := os.Open("/opt/in")
	cmd.Stdin = inFile

	outFile, _ := os.Create("/opt/out")
	cmd.Stdout = outFile

	startTime = time.Now()
	cmd.Run()
	endTime = time.Now()

	type InfoJSON struct {
		RealTime   time.Duration `json:"real_time"`
		UserTime   time.Duration `json:"user_time"`
		SystemTime time.Duration `json:"sys_time"`
		MaxRSS     int64         `json:"max_rss"`
	}

	out := InfoJSON{
		RealTime:   endTime.Sub(startTime),
		UserTime:   cmd.ProcessState.UserTime(),
		SystemTime: cmd.ProcessState.SystemTime(),
		MaxRSS:     cmd.ProcessState.SysUsage().(*syscall.Rusage).Maxrss,
	}

	outJSON, _ := json.Marshal(out)
	fmt.Fprintf(os.Stdout, "%s", string(outJSON))
	fmt.Fprintf(infoFile,"%s",string(outJSON))

	return
}
